id: n5imv64i14btgrqm6qbzyhqs2xc2e7w3hxk1l9b9s5qmtfz0
name: yet-another-pomf-clone
main: src/main.zig
dev_dependencies:

  - src: git https://github.com/lun-4/apple_pie
    name: apple_pie
    main: src/apple_pie.zig

  - src: git https://github.com/truemedian/hzzp
    name: hzzp
    main: src/main.zig

  - src: git https://github.com/frmdstryr/zig-mimetypes
    name: mimetypes
    main: mimetypes.zig
