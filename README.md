# yet-another-pomf-clone

## how build

get [zigmod](https://github.com/nektro/zigmod).

```
git clone ...
cd yet-another-pomf-clone
zigmod fetch
zig build
./zig-cache/bin/yet-another-pomf-clone
```

happy webscale

### todo

- multipart parsing (this is going to be fun)
- env vars for configuration
- handle 404 on fetch

## how use

do the thing

```
curl -F "test=@/path/to/file/lol" 'http://localhost:8080/api/upload'
```
